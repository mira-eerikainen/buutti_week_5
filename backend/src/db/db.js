import pg from "pg";
import dotenv from "dotenv";

// pg.defaults.ssl = true; // ?

const isProd = process.env.NODE_ENV === "prod";

!isProd && dotenv.config();

const { APP_PG_HOST, APP_PG_USER, APP_PG_DB, APP_PG_PASSWORD, APP_PG_PORT } = process.env;

export const pool = new pg.Pool({
  host: APP_PG_HOST,
  user: APP_PG_USER,
  database: APP_PG_DB,
  password: APP_PG_PASSWORD,
  port: APP_PG_PORT,
  ssl: isProd
});

// Execute query
const executeQuery = async (query, params) => {
  const client = await pool.connect();
  try {
    const result = await client.query(query, params);
    return result;
  } catch (error) {
    console.error(error.stack);
    error.name = "dbError";
    throw error;
  } finally {
    client.release();
  }
};

const createBookTable = `
    CREATE TABLE IF NOT EXISTS "books" (
	    "id" VARCHAR(36) NOT NULL,
	    "name" VARCHAR(100) NOT NULL,
      "author" VARCHAR(100) NOT NULL,
	    "read" BOOLEAN NOT NULL,
	    PRIMARY KEY ("id")
    );`;

// Create tables
const createTables = async () => { 
  await Promise.all([
    await executeQuery(createBookTable),
  ]);
  console.log("Tables initialized.");
};

export default { 
  executeQuery,
  createTables,
};
