import { Router } from "express";
import booksService from "../services/booksService.js";

const router = Router();

// Add a book
router.post("/", async (req, res) => {
  const book = req.body;
  const storedItem = await booksService.addBook(book);
  if (storedItem.rowCount === 1) {
    res.status(201).send({message: "New item added.", id: storedItem.rows[0].id});
  } else {
    throw new Error("Error");
  }
});

// Find all books
router.get("/", async (_req, res) => {
  const books = await booksService.findAllBooks();
  res.status(200).json(books);
});

// Find one book
router.get("/:id", async (req, res) => {
  const book = await booksService.findOneBook(req.params.id);
  res.status(200).json(book);
});

// Delete a book
router.delete("/:id", async (req, res) => {
  const id = req.params.id;
  if (id) {
    await booksService.deleteBook(id);
    res.status(200).send("Item deleted.");
  } else {
    throw new Error("No item with requested id#.");
  }
});

// Edit details of a book
router.put("/:id", async (req, res) => {
  const book = { id: req.params.id, ...req.body };
  const storedBook = await booksService.updateBook(book);
  if (storedBook.rowCount === 1) {
    res.status(200).send("Item updated.");
  } else {
    throw new Error("Error");
  }
});

export default router;
