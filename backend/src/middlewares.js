export const unknownEndpoint = (_req, res) => {
  res.status(404).send({ error: "No one here" });
};

export const errorHandler = (error, _req, res, next) => {
  if (error.name === "dbError") {
    res.status(500).send({ error: error.message });
  } else if (error.name === "UserError") {
    res.status(400).send({ error: error.message });
  }
  console.error(error);
  next(error);
};

export const allowCors = (_req, res, next) => {
  res.header("Access-Control-Allow-Origin", "localhost");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
};