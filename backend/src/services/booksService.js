import booksDao from "../dao/booksDao.js";

const validateItem = (book) => {
  return book["name"] && book["author"];
};

const addBook = async (book) => {
  if (!validateItem(book)) {
    const err = new Error("Invalid input");
    err.name = "UserError";
    throw err;
  } else {
    return await booksDao.addBook(book);
  }
};

const findAllBooks = async () => {
  const books = await booksDao.findAllBooks();
  return books.rows;
};

const findOneBook = async (id) => {
  const book = await booksDao.findOneBook(id);
  return book;
};

const deleteBook = async (book) => {
  await booksDao.deleteBook(book);
  return;
};

const updateBook = async (book) => {
  if (!validateItem(book) || !book.id) {
    const err = new Error("Invalid id#.");
    err.name = "UserError";
    throw err;
  } else {
    return await booksDao.updateBook(book);
  }
};

export default { 
  addBook, 
  findAllBooks, 
  findOneBook,
  deleteBook,
  updateBook
};

