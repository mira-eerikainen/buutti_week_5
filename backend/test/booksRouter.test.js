/* eslint-disable no-undef */
import { jest } from "@jest/globals";
import request from "supertest";
import app from "../src/index.js";
import { pool } from "../src/db/db.js";

const initializeDbMock = (expectedResponse) => {
  pool.connect = jest.fn(() => {
    return {
      query: () => expectedResponse,
      release: () => null
    };
  });
};


describe("Testing API", () => {
  it("Root responds OK", async () => {
    const response = await request(app).get("/");
    expect(response.status).toBe(200);
  });
});

describe("Testing GET /books", () => {

  const mockResponse = { 
    rows: [
      { id: 12345, name: "kirja1", author: "kirjailija1", read: false },
      { id: 54321, name: "kirja2", author: "kirjailija2", read: false }
    ] 
  };

  beforeAll(() => {
    initializeDbMock(mockResponse);
  });

  afterAll(() => {
    jest.clearAllMocks();
  });

  it("Returns 200 with all books", async () => {
    const response = await request(app).get("/books");
    expect(response.status).toBe(200);
    expect(JSON.parse(response.text)).toStrictEqual(mockResponse.rows);
  });

  it("Returns 500 with a single product when returning multiple books", async () => {
    const response = await request(app).get("/books/12345");
    expect(response.status).toBe(500);
  });
}); 

describe("Testing GET /books/id", () => {

  it("Returns 200 with a single item", async () => {
    const mockResponse = {
      rowCount: 1,
      rows: [
        { id: 54321, name: "kirja2", author: "kirjailija2", read: false }
      ] 
    };
    initializeDbMock(mockResponse);
    const response = await request(app).get("/books/54321");
    expect(response.status).toBe(200);
    expect(JSON.parse(response.text)).toStrictEqual(mockResponse.rows);
  });
});
