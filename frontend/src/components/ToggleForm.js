import Button from "./Button";

const ToggleForm = ({ onAdd, showAdd }) => {
  return (
    <div className="container btn-container">
      <Button
        text={showAdd ? "Cancel" : "Add a book"}
        onClick={onAdd}
      />
    </div>
  );
};

export default ToggleForm;