import Book from "./Book";

const Books = ({ books, handleDelete }) => {

  return (
    <>
      <div className="container">
        {books.map((book, index) => (
          <Book 
            key={index}
            id={`${book.name}_${book.id}`}
            book={book} 
            handleDelete={handleDelete}/>
        ))}
      </div>
    </>
  );
};

export default Books;