const Book = ( { book, handleDelete }) => {
  return (
    <div className="book-container">
      <div className="book-item">
        <p>
          <span>Title: </span>{book.name}
        </p>
      </div>
      <div className="book-item">
        <p>
          <span>Author: </span>{book.author}
        </p>
      </div>
      <div className="btn-container">
        <button 
          className="btn delete"
          onClick={() => handleDelete(book.id)}>
          Delete
        </button>
      </div>
    </div>
  );
};

export default Book;