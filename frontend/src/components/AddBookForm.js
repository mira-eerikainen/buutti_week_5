import { useState } from "react";

const AddBookForm = ({ onAdd }) => {
  const [name, setName] = useState("");
  const [author, setAuthor] = useState("");
  const [read, setRead] = useState(false);

  const onSubmit = (e) => {
    e.preventDefault();

    if (!name) {
      alert("Please add a title");
      return;
    }

    if (!author) {
      alert("Please add an author");
      return;
    }

    onAdd({ name, author, read });

    setName("");
    setAuthor("");
    setRead(false);
  };
  
  return (
    <div className="container">
      <form className="form-container" onSubmit={onSubmit}>
        <div className="form-control">
          <label>Book title</label>
          <input 
            type="text"
            placeholder="Name of the book"
            value={name}
            onChange={(e) => setName(e.target.value)}
          />
        </div>
        <div className="form-control">
          <label>Author</label>
          <input 
            type="text"
            placeholder="Last name, First name"
            value={author}
            onChange={(e) => setAuthor(e.target.value)}
          />
        </div>
        <div className="form-control-check-box">
          <input
            className="check-box"
            type='checkbox'
            checked={read}
            value={read}
            onChange={(e) => setRead(e.currentTarget.checked)}
          />
          <label>I have read this book</label>

        </div>
        <input type="submit" value="Save" className="btn btn-save" />
      </form>
    </div>
  );
};

export default AddBookForm;